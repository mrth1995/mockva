package com.daksa.mockva.application;

import com.daksa.mockva.application.exception.AccountAlreadyRegisteredException;
import com.daksa.mockva.application.exception.AccountDoesNotExistException;
import com.daksa.mockva.domain.Account;
import com.daksa.mockva.domain.AccountRepository;
import com.daksa.mockva.rest.AccountModel;
import javax.persistence.EntityManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author userr
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

	@InjectMocks
	private AccountService service;
	@Mock
	private EntityManager em;
	@Mock
	private AccountRepository accountRepository;
			
	public AccountServiceTest() {
	}
	/**
	 * @throws java.lang.Exception
	 */
	@Test
	public void testRegister() throws Exception {
		System.out.println("register account");
		AccountModel accountModel = new AccountModel();
		accountModel.setId("001");
		Account account = service.register(accountModel);
		Assert.assertEquals("001", account.getId());
		verify(em).persist(account);
	}
	
	@Test(expected = AccountAlreadyRegisteredException.class)
	public void testRegister_AlreadyRegistered() throws Exception {
		System.out.println("register account already registered");
		Account account = new Account("001", "Account 001", null, null, Boolean.FALSE);
		when(accountRepository.getById("001")).thenReturn(account);
		AccountModel accountModel = new AccountModel();
		accountModel.setId("001");
		service.register(accountModel);
	}

	@Test
	public void testDelete() throws AccountDoesNotExistException{
		System.out.println("delete account");
		
		Account account = new Account("001", "Account 001", null, null, Boolean.FALSE);
		when(accountRepository.getById("001")).thenReturn(account);
		AccountModel accountModel = new AccountModel();
		accountModel.setId("001");
		Account delete = service.delete(accountModel);
		verify(em).remove(delete);
	}
	
	@Test(expected = AccountDoesNotExistException.class)
	public void testDelete_AccountDoesNotExistException() throws AccountDoesNotExistException{
		System.out.println("delete account exception");
		
		when(accountRepository.getById("001")).thenReturn(null);
		AccountModel accountModel = new AccountModel();
		accountModel.setId("001");
		service.delete(accountModel);
	}
	
	@Test
	public void testUpdate() throws Exception{
		System.out.println("update account");
		AccountModel accountModel = new AccountModel();
		accountModel.setId("001");
		accountModel.setName("account 001");
		accountModel.setAddress("address 002");
		Account account = new Account("001", "account 001", "address 001", null, Boolean.FALSE);
		when(accountRepository.getById("001")).thenReturn(account);
		
		Account result = service.update(accountModel);
		Assert.assertEquals("address 002", result.getAddress());
		verify(em).merge(account);
	}
	
	@Test(expected = AccountDoesNotExistException.class)
	public void testUpdate_AccountDoesNotExistException() throws Exception{
		System.out.println("update account exception");
		AccountModel accountModel = new AccountModel();
		accountModel.setId("001");
		accountModel.setName("account 001");
		accountModel.setAddress("address 002");
		when(accountRepository.getById("001")).thenReturn(null);
		
		service.update(accountModel);
	}
}
