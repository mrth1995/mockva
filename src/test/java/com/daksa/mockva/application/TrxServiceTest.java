package com.daksa.mockva.application;

import com.daksa.mockva.application.exception.AccountDoesNotExistException;
import com.daksa.mockva.application.exception.CannotHaveNegativeBalanceException;
import com.daksa.mockva.application.exception.CannotTransferToTheSameAccount;
import com.daksa.mockva.domain.Account;
import com.daksa.mockva.domain.AccountRepository;
import com.daksa.mockva.domain.Trx;
import com.daksa.mockva.rest.TrxModel;
import java.math.BigDecimal;
import javax.persistence.EntityManager;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Ridwan
 */

@RunWith(MockitoJUnitRunner.class)
public class TrxServiceTest {
	@InjectMocks TrxService service;
	@Mock EntityManager em;
	@Mock AccountRepository accountRepository;
	
	public TrxServiceTest() {
	}

	@Test
	public void testTransfer() throws Exception{
		System.out.println("transfer negative balance true");
		TrxModel trxModel = new TrxModel();
		trxModel.setSrcId("001");
		trxModel.setDstId("002");
		trxModel.setAmount(new BigDecimal(2000));
		
		Account src = new Account();
		src.setId("001");
		src.setName("acc001");
		src.setBalance(new BigDecimal(10000));
		src.setNegativeBalance(Boolean.FALSE);
		
		Account dst = new Account("002");
		dst.setId("002");
		dst.setName("acc002");
		dst.setBalance(new BigDecimal(10000));
		dst.setNegativeBalance(Boolean.FALSE);
		
		when(accountRepository.getById("001")).thenReturn(src);
		when(accountRepository.getById("002")).thenReturn(dst);
		
		Trx result = service.transfer(trxModel);
		
		Account srcResult = accountRepository.getById("001");
		Account dstResult = accountRepository.getById("002");
		assertEquals(new BigDecimal(8000), srcResult.getBalance());
		assertEquals(new BigDecimal(12000), dstResult.getBalance());
		verify(em).merge(src);
		verify(em).merge(dst);
		verify(em).persist(result);
	}
	
	@Test(expected = CannotHaveNegativeBalanceException.class)
	public void testTransfer_NegativeBalanceFalse() throws Exception{
		System.out.println("transfer negative balance false");
		TrxModel trxModel = new TrxModel();
		trxModel.setSrcId("001");
		trxModel.setDstId("002");
		trxModel.setAmount(new BigDecimal(2000));
		Account src = new Account();
		src.setId("001");
		src.setName("acc001");
		src.setBalance(new BigDecimal(1000));
		src.setNegativeBalance(Boolean.FALSE);
		
		Account dst = new Account("002");
		dst.setId("002");
		dst.setName("acc002");
		dst.setBalance(new BigDecimal(10000));
		when(accountRepository.getById("001")).thenReturn(src);
		when(accountRepository.getById("002")).thenReturn(dst);
		
		service.transfer(trxModel);
	}
	
	@Test(expected = CannotTransferToTheSameAccount.class)
	public void testTransfer_TrfToSameAccount() throws Exception{
		System.out.println("transfer to same account");
		TrxModel trxModel = new TrxModel();
		trxModel.setSrcId("001");
		trxModel.setDstId("001");
		trxModel.setAmount(new BigDecimal(2000));
		
		service.transfer(trxModel);
	}
	
	@Test(expected = AccountDoesNotExistException.class)
	public void testTransfer_AccountDoesNotExist() throws Exception{
		System.out.println("account does not exist");
		TrxModel trxModel = new TrxModel();
		trxModel.setSrcId("001");
		trxModel.setDstId("002");
		trxModel.setAmount(new BigDecimal(2000));
		
		Account dst = null;
		
		when(accountRepository.getById("002")).thenReturn(dst);
		
		service.transfer(trxModel);
	}
	
}