package com.daksa.mockva.infrastructure;

import com.daksa.mockva.domain.Trx;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Ridwan
 */
@RunWith(MockitoJUnitRunner.class)
public class TrxRepositoryImplTest {
	
	public TrxRepositoryImplTest() {
	}
	@InjectMocks TrxRepositoryImpl repo;
	@Mock EntityManager em;
	
	@Test
	public void testGetAll(){
		System.out.println("test get all trx");
		List<Trx> listTrx = mock(ArrayList.class);
		TypedQuery q = mock(TypedQuery.class);
		when(em.createNamedQuery("Trx.findAll", Trx.class)).thenReturn(q);
		when(q.getResultList()).thenReturn(listTrx);
		List<Trx> result = repo.getAllData();
		assertEquals(result, listTrx);
	}
	
}
