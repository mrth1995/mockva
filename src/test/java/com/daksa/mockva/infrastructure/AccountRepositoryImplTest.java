package com.daksa.mockva.infrastructure;

import com.daksa.mockva.domain.Account;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import org.junit.Test;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Ridwan
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountRepositoryImplTest {
	@InjectMocks
	AccountRepositoryImpl repo;
	@Mock
	EntityManager em;

	@Test
	public void testGetAllData() {
		System.out.println("get all account");
		List<Account> listAccount = mock(ArrayList.class);
		TypedQuery q = mock(TypedQuery.class);
		when(em.createNamedQuery("Account.findAll", Account.class)).thenReturn(q);
		when(q.getResultList()).thenReturn(listAccount);
		List<Account> result = repo.getAllData();
		assertEquals(result, listAccount);
	}
	
	@Test
	public void testFindById(){
		System.out.println("find by id");
		Account acc = new Account("001");
		when(em.find(Account.class, "001")).thenReturn(acc);
		Account result = repo.getById("001");
		assertEquals(acc, result);
	}
	
}
