package com.daksa.mockva.web;

import com.daksa.mockva.application.AccountService;
import com.daksa.mockva.application.TrxService;
import com.daksa.mockva.application.exception.AccountDoesNotExistException;
import com.daksa.mockva.application.exception.CannotHaveNegativeBalanceException;
import com.daksa.mockva.application.exception.CannotTransferToTheSameAccount;
import com.daksa.mockva.domain.Account;
import com.daksa.mockva.domain.AccountRepository;
import com.daksa.mockva.domain.Trx;
import com.daksa.mockva.domain.TrxRepository;
import com.daksa.mockva.rest.AccountModel;
import com.daksa.mockva.rest.TrxModel;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;

/**
 *
 * @author Ridwan
 */
@Named
@ViewScoped
public class IndexBacking implements Serializable {

	@Inject
	private AccountService accService;
	@Inject
	private TrxService trxService;
	@Inject
	private AccountRepository accRepository;
	@Inject
	private TrxRepository trxRepository;

	private AccountModel acc = new AccountModel();

	private TrxModel trx = new TrxModel();

	private String dstName;

	private String srcName;

	private BigDecimal amount;

	private List<Account> listAcc = new ArrayList<Account>();

	private List<Trx> listTrx = new ArrayList<Trx>();

	private LineChartModel chart;

	@PostConstruct
	public void getAllData() {

		listAcc = accRepository.getAllData();
		listTrx = trxRepository.getAllData();
		acc = new AccountModel();
		createChart();
	}

	public void createChart() {
		chart = initCategoryModel();
		chart.setTitle("Transactional Daily Report");
		chart.setLegendPosition("e");

		DateAxis axis = new DateAxis("Dates");
		axis.setTickAngle(-50);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateMin = sdf.format(listTrx.get(listTrx.size() - 1).getTrxTimestamp());
		axis.setMin(dateMin);

		String dateMax = sdf.format(listTrx.get(0).getTrxTimestamp());
		

		if (dateMax.equals(dateMin)) {
			Date d = listTrx.get(listTrx.size() - 1).getTrxTimestamp();
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			c.add(Calendar.DATE, 1);
			d = c.getTime();
			dateMax = sdf.format(d);
		}
		axis.setMax(dateMax);
		axis.setTickFormat("%b %#d, %y");
		chart.getAxes().put(AxisType.X, axis);
	}

	public LineChartModel initCategoryModel() {
		LineChartModel model = new LineChartModel();

		ChartSeries chartSeries = new ChartSeries();

		chartSeries.setLabel("amount");
		List<Trx> chartData = new ArrayList<Trx>();
		int index = 0;
		for (int i = listTrx.size() - 1; i >= 0; i--) {
			if (i == listTrx.size() - 1) {
				chartData.add(listTrx.get(i));
			} else {
				String chartDataDate = new SimpleDateFormat("d-MMM-yyyy").format(chartData.get(index).getTrxTimestamp());
				String listDataDate = new SimpleDateFormat("d-MMM-yyyy").format(listTrx.get(i).getTrxTimestamp());
				if (!chartDataDate.equals(listDataDate)) {
					chartData.add(listTrx.get(i));
					index = chartData.indexOf(listTrx.get(i));
				} else {
					BigDecimal chartDataAmount = chartData.get(index).getAmount();
					Trx trxtemp = new Trx(listTrx.get(i).getAccountSrc(), listTrx.get(i).getAccountDst(), chartDataAmount.add(listTrx.get(i).getAmount()), listTrx.get(i).getTrxTimestamp());
					chartData.set(index, trxtemp);
				}
			}
		}
		for (Trx data : chartData) {
			String date = new SimpleDateFormat("d-MMM-yyyy").format(data.getTrxTimestamp());
			chartSeries.set(date, data.getAmount());
		}
		model.addSeries(chartSeries);
		return model;
	}

	public Trx getLastTrx(Account a) {
		List<Trx> listAccTrx = a.getTrx();
		Trx lastTrx = listAccTrx.get(listAccTrx.size() - 1);
		return lastTrx;
	}

	public void register() throws Exception {
		try {
			accService.register(acc);
			addMessage("Success", "New Account has been registered successfully", FacesMessage.SEVERITY_INFO);
			acc = new AccountModel();
			getAllData();
		} catch (Exception e) {
			addMessage("Failed", e.getMessage(), FacesMessage.SEVERITY_WARN);
		}

	}

	public void setTrxDstName(String id) {
		for (Account a : listAcc) {
			if (a.getId().equals(id)) {
				dstName = a.getName();
				break;
			} else {
				dstName = null;
			}
		}
	}

	public void setTrxSrc(Account acc) {
		trx.setSrcId(acc.getId());
		setSrcName(acc.getName());
	}

	public void transfer() throws Exception {
		try {
			trxService.transfer(trx);
			addMessage("Success", "Transaction success", FacesMessage.SEVERITY_INFO);
			getAllData();
			trx = new TrxModel();
		} catch (CannotHaveNegativeBalanceException e) {
			addMessage("Fail", "Account '" + trx.getSrcId() + "' " + e.getMessage(), FacesMessage.SEVERITY_WARN);
		} catch (CannotTransferToTheSameAccount | AccountDoesNotExistException e) {
			addMessage("Fail", e.getMessage(), FacesMessage.SEVERITY_WARN);
		}

	}

	public void addMessage(String summary, String detail, FacesMessage.Severity type) {
		FacesMessage message = new FacesMessage(type, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public TimeZone getTimeZone() {
		return TimeZone.getDefault();
	}

	public Locale getLocale() {
		return new Locale("en", "US");
	}

	public AccountModel getAcc() {
		return acc;
	}

	public void setAcc(AccountModel acc) {
		this.acc = acc;
	}

	public List<Account> getListAcc() {
		return listAcc;
	}

	public void setListAcc(List<Account> listAcc) {
		this.listAcc = listAcc;
	}

	public List<Trx> getListTrx() {
		return listTrx;
	}

	public void setListTrx(List<Trx> listTrx) {
		this.listTrx = listTrx;
	}

	public TrxModel getTrx() {
		return trx;
	}

	public void setTrx(TrxModel trx) {
		this.trx = trx;
	}

	public LineChartModel getChart() {
		return chart;
	}

	public void setChart(LineChartModel chart) {
		this.chart = chart;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getDstName() {
		return dstName;
	}

	public void setDstName(String dstName) {
		this.dstName = dstName;
	}

	public String getSrcName() {
		return srcName;
	}

	public void setSrcName(String srcName) {
		this.srcName = srcName;
	}

}
