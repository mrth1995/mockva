package com.daksa.mockva.rest;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Ridwan
 */
public class TrxModel implements Serializable{
	private BigDecimal amount;
	private String srcId;
	private String dstId;
	public TrxModel() {
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getSrcId() {
		return srcId;
	}

	public void setSrcId(String srcId) {
		this.srcId = srcId;
	}

	public String getDstId() {
		return dstId;
	}

	public void setDstId(String dstId) {
		this.dstId = dstId;
	}
	
	
	
}