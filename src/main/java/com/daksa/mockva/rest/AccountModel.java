package com.daksa.mockva.rest;

import java.util.Date;

/**
 *
 * @author Ridwan
 */
public class AccountModel {
	private String id;
	private String name;
	private String address;
	private Date birth;
	private Boolean negativeBalance;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public Boolean getNegativeBalance() {
		return negativeBalance;
	}

	public void setNegativeBalance(Boolean negativeBalance) {
		this.negativeBalance = negativeBalance;
	}
	
	
}