package com.daksa.mockva.rest;

import com.daksa.mockva.application.TrxService;
import com.daksa.mockva.domain.Trx;
import com.daksa.mockva.domain.TrxRepository;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Ridwan
 */
@RequestScoped
@Path("trx")
public class TrxResource {
	@Inject
	private TrxRepository trxRepo;
	@Inject
	private TrxService trxService;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response transfer(TrxModel trxModel){
		System.out.println("in trx resource");
		try {
			trxService.transfer(trxModel);
			return Response.status(201).build();
		} catch (Exception e) {
			return Response.status(400).build();
		}
	}
	
	@GET
	@Path("/transactions")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Trx> getAllTrx(){
		return trxRepo.getAllData();
	}
}