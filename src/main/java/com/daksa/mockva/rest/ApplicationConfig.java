package com.daksa.mockva.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Ridwan
 */
@ApplicationPath("rest")
public class ApplicationConfig extends Application{
	
}