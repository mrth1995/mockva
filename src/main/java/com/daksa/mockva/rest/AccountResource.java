package com.daksa.mockva.rest;

import com.daksa.mockva.application.AccountService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.daksa.mockva.domain.Account;
import com.daksa.mockva.domain.AccountRepository;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author Ridwan
 */
@RequestScoped
@Path("account")
public class AccountResource {

	@Inject
	private AccountRepository accRepo;
	@Inject
	private AccountService accService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response register(AccountModel a) throws Exception {
		try {
			accService.register(a);
			return Response.status(201).build();
		} catch (RuntimeException e) {
			return Response.status(400).build();
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Account> getAccounts() {
		return accRepo.getAllData();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(AccountModel accountModel){
		try {
			accService.update(accountModel);
			return Response.status(200).entity("Account updated").build();
		} catch (Exception e) {
			return Response.status(400).entity("Account failed to update").build();
		}
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public Response delete(AccountModel accountModel){
		try {
			accService.delete(accountModel);
			return Response.status(200).entity("Account deleted").build();
		} catch (Exception e) {
			return Response.status(400).entity("Delete failed").build();
		}
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Account getAccounts(@PathParam("id") String id) {
		return accRepo.getById(id);
	}
}
