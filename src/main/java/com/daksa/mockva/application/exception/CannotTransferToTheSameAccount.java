package com.daksa.mockva.application.exception;

/**
 *
 * @author Ridwan
 */
public class CannotTransferToTheSameAccount extends Exception {

	public CannotTransferToTheSameAccount() {
		super("Cannot transfer to the same account");
	}
	
}