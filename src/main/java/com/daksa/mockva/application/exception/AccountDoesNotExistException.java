package com.daksa.mockva.application.exception;

/**
 *
 * @author salman
 */
public class AccountDoesNotExistException extends Exception{

	public AccountDoesNotExistException(String id) {
		super("Account with ID : " + id + " does not exist");
	}
	
}