package com.daksa.mockva.application.exception;

/**
 *
 * @author Ridwan
 */
public class AccountAlreadyRegisteredException extends Exception{

	
	public AccountAlreadyRegisteredException() {
		super("Account already registered");
	}
}