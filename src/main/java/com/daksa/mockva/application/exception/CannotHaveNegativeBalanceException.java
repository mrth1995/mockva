package com.daksa.mockva.application.exception;

/**
 *
 * @author Ridwan
 */
public class CannotHaveNegativeBalanceException extends Exception{

	public CannotHaveNegativeBalanceException() {
		super("cannot have negative balance");
	}
	
}