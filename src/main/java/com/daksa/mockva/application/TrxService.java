package com.daksa.mockva.application;

import com.daksa.mockva.application.exception.AccountDoesNotExistException;
import com.daksa.mockva.application.exception.CannotHaveNegativeBalanceException;
import com.daksa.mockva.application.exception.CannotTransferToTheSameAccount;
import com.daksa.mockva.domain.Account;
import com.daksa.mockva.domain.AccountRepository;
import com.daksa.mockva.domain.Trx;
import com.daksa.mockva.rest.TrxModel;
import java.io.Serializable;
import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ridwan
 */
@Stateless
public class TrxService implements Serializable {
	@Inject
	private EntityManager entityManager;
	@Inject
	private AccountRepository accRepo;

	public Trx transfer(TrxModel trxModel) throws Exception {
		Account src = accRepo.getById(trxModel.getSrcId());
		Account dst = accRepo.getById(trxModel.getDstId());
		if (trxModel.getSrcId().equals(trxModel.getDstId())) {
			throw new CannotTransferToTheSameAccount();
		}
		if (dst == null || trxModel.getDstId().isEmpty()) {
			throw new AccountDoesNotExistException(trxModel.getDstId());
		}
		if (src.getNegativeBalance() == false && src.getBalance().compareTo(trxModel.getAmount()) <= 0) {
			throw new CannotHaveNegativeBalanceException();
		}
		Date timestamp = new Date();
		Trx trx = new Trx(src, dst, trxModel.getAmount(),timestamp);
		src.setBalance(src.getBalance().subtract(trxModel.getAmount()));
		dst.setBalance(dst.getBalance().add(trxModel.getAmount()));
		entityManager.merge(src);
		entityManager.merge(dst);
		entityManager.persist(trx);
		return trx;

	}
}
