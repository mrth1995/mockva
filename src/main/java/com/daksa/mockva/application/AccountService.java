package com.daksa.mockva.application;

import com.daksa.mockva.application.exception.AccountAlreadyRegisteredException;
import com.daksa.mockva.application.exception.AccountDoesNotExistException;
import com.daksa.mockva.domain.Account;
import com.daksa.mockva.domain.AccountRepository;
import com.daksa.mockva.rest.AccountModel;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ridwan
 */

@Stateless

public class AccountService implements Serializable{
	@Inject
	private EntityManager em;
	
	@Inject
	private AccountRepository accountRepository;
	
	public Account register(AccountModel accModel) throws Exception{
		Account account = accountRepository.getById(accModel.getId());
		if (account != null) {
			throw new AccountAlreadyRegisteredException();
		}
		account = new Account(accModel.getId(), accModel.getName(), accModel.getAddress(), accModel.getBirth(), accModel.getNegativeBalance());
		em.persist(account);
		return account;
	}
	
	public Account update(AccountModel accountModel) throws Exception{
		Account account = accountRepository.getById(accountModel.getId());
		if (account == null) {
			throw new AccountDoesNotExistException(accountModel.getId());
		}
		account.setAddress(accountModel.getAddress());
		account.setBirth(accountModel.getBirth());
		em.merge(account);
		return account;
	}
	
	public Account delete(AccountModel accountModel) throws AccountDoesNotExistException{
		Account account = accountRepository.getById(accountModel.getId());
		if (account == null) {
			throw new AccountDoesNotExistException(accountModel.getId());
		}
		em.remove(account);
		return account;
	}
}