package com.daksa.mockva.infrastructure;

import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Ridwan
 */

@Dependent
public class EntityManagerProducer implements Serializable {
	
	@PersistenceContext(
			unitName = "mockva_pu"
	)
	
	private EntityManager entityManager;

	@Produces
	public EntityManager getEntityManager() {
		
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
}