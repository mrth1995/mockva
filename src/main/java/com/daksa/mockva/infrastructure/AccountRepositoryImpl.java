package com.daksa.mockva.infrastructure;

import com.daksa.mockva.domain.Account;
import com.daksa.mockva.domain.AccountRepository;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ridwan
 */
@Dependent
public class AccountRepositoryImpl implements Serializable, AccountRepository{
	@Inject
	private EntityManager entityManager;
	@Override
	public List<Account> getAllData(){
		return entityManager.createNamedQuery("Account.findAll",Account.class).getResultList();
	}
	@Override
	public Account getById(String id){
		return entityManager.find(Account.class, id);
	}
}