package com.daksa.mockva.infrastructure;

import com.daksa.mockva.domain.Trx;
import com.daksa.mockva.domain.TrxRepository;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ridwan
 */
@Dependent
public class TrxRepositoryImpl implements Serializable, TrxRepository{
	@Inject
	private EntityManager entityManager;
	
	@Override
	public List<Trx> getAllData(){
		return entityManager.createNamedQuery("Trx.findAll",Trx.class).getResultList();
	}
}