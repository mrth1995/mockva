package com.daksa.mockva.infrastructure.rest;

import java.io.IOException;
import java.io.InputStream;
import javax.enterprise.context.RequestScoped;
import javax.naming.AuthenticationException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.HmacUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ridwan
 */
@WebFilter(filterName = "Authorization")
@RequestScoped
public class RequestFilter implements Filter {

	private final static Logger LOG = LoggerFactory.getLogger(RequestFilter.class);
	private final static String AUTH_MODE_DA01 = "DA01";

	private FilterConfig filterConfig;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String resource = getResource(httpRequest);
		
		try {
			String authorization = httpRequest.getHeader("Authorization");
			String apiKey = null, signature = null;
			if (authorization != null) {
				{
					String[] authPart = authorization.split("\\s");
					if (authPart.length < 2) {
						throw new AuthenticationException("Authorization header field is not valid");
					}
					String[] authPart2 = authPart[1].split(":");
					String authMode = authPart[0];
					if (!authMode.equals(AUTH_MODE_DA01)) {
						throw new AuthenticationException("Unsupported authentication type");
					}
					if (authPart2 == null || authPart2.length < 2 || !authMode.equals(AUTH_MODE_DA01)) {
						throw new AuthenticationException("Signature format is not valid");
					}
					//api key sama dengan username di client
					apiKey = authPart2[0];
					signature = authPart2[1];
				}
				//password ini nanti bisa dicari di repository berdasarkan username
				String password = "password";
				String generatedSignature = generateSignature(httpRequest, resource, password);
				LOG.debug("signature from server : " + generatedSignature);
				if (!generatedSignature.equals(signature)) {
					throw new AuthenticationException("Signature is not match");
				}
				chain.doFilter(request, response);
			}
		} catch (AuthenticationException e) {
			throw new ServletException(e);
		}
	}

	private String generateSignature(HttpServletRequest httpRequest, String canonicalPath, String password) throws IOException {
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(httpRequest.getMethod()).append("\n");
		if (httpRequest.getContentLength() > 0) {
			signatureBuilder.append(contentMD5(httpRequest)).append("\n");
		}
		if (httpRequest.getContentType() != null) {
			signatureBuilder.append(httpRequest.getContentType()).append("\n");
		}
		signatureBuilder.append(httpRequest.getHeader("Date")).append("\n");
		signatureBuilder.append(canonicalPath);
		String digest = signatureBuilder.toString();
		String signature = Base64.encodeBase64String(HmacUtils.hmacSha1(password, digest));
		return signature;
	}

	private String contentMD5(HttpServletRequest httpRequest) throws IOException {
		try (InputStream input = httpRequest.getInputStream()) {
			String contentHashed = DigestUtils.md5Hex(input);
			return contentHashed;
		}
	}

	private String getResource(HttpServletRequest httpServletRequest) {
		String resource = httpServletRequest.getPathInfo();
		if (!resource.startsWith("/")) {
			resource += "/" + resource;
		}
		return resource;
	}

	@Override
	public void destroy() {

	}

}
