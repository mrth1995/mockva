package com.daksa.mockva.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Ridwan
 */
@Entity
@Table(name = "account")
@NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a ORDER BY a.id ASC")

public class Account implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 3)
	@Column(name = "acc_id")
	private String id;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 30)
	@Column(name = "name")
	private String name;

	@Size(max = 2147483647)
	@Column(name = "address")
	private String address;

	@Column(name = "birth")
	@Temporal(TemporalType.DATE)
	private Date birth;

	@Column(name = "negative_balance")
	private Boolean negativeBalance;

	@Column(name = "balance")
	private BigDecimal balance = BigDecimal.ZERO;
	
	
	@OneToMany(mappedBy = "accountSrc", targetEntity = Trx.class, fetch = FetchType.EAGER)
	@OrderBy("trx_timestamp ASC")
	private List<Trx> trx;

	public Account() {
		this.balance = BigDecimal.ZERO;
	}

	public Account(String accId) {
		this.id = accId;
		this.balance = BigDecimal.ZERO;
	}

	public Account(String accId, String name, String address, Date birth, Boolean negativeBalance) {
		this.id = accId;
		this.name = name;
		this.address = address;
		this.balance = BigDecimal.ZERO;
		this.birth = birth;
		this.negativeBalance = negativeBalance;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public Date getBirth() {
		return birth;
	}

	public Boolean getNegativeBalance() {
		return negativeBalance;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public void setNegativeBalance(Boolean negativeBalance) {
		this.negativeBalance = negativeBalance;
	}

	@XmlTransient
	public List<Trx> getTrx() {
		return trx;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
}
