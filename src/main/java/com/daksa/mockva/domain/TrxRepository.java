package com.daksa.mockva.domain;

import java.util.List;

/**
 *
 * @author Ridwan
 */
public interface TrxRepository {
	public List<Trx> getAllData();
}
