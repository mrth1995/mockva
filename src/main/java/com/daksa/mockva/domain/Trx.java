package com.daksa.mockva.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ridwan
 */
@Entity
@Table(name = "trx")
@NamedQuery(name = "Trx.findAll", query = "SELECT t FROM Trx t ORDER BY t.trxTimestamp DESC")

public class Trx implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 30)
	@Column(name = "id")
	private String id;

	@Basic(optional = false)
	@NotNull
	@Column(name = "trx_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date trxTimestamp;

	@Column(name = "amount")
	private BigDecimal amount;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="trx_dst", referencedColumnName = "acc_id")
	private Account accountDst;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="trx_src", referencedColumnName = "acc_id")
	private Account accountSrc;

	public Trx() {
	}
	
	public Trx(Account src, Account dst, BigDecimal amount, Date timestamp) {
		this.id = generateIdTrx().concat(":").concat(src.getId()).concat(":").concat(dst.getId());
		this.trxTimestamp = timestamp;
		this.amount = amount;
		this.accountSrc = src;
		this.accountDst = dst;
	}

	public static String generateIdTrx() {
		StringBuilder sb = new StringBuilder(13);
		sb.append(System.currentTimeMillis());

		while (sb.length() < 13) {
			sb.append((char) ((Math.random() * 10) + '0'));
		}

		return sb.toString();
	}

	public String getId() {
		return id;
	}

	public Date getTrxTimestamp() {
		return trxTimestamp;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public Account getAccountSrc() {
		return accountSrc;
	}

	public Account getAccountDst() {
		return accountDst;
	}
	
}
