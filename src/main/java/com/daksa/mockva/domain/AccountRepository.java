package com.daksa.mockva.domain;

import java.util.List;

/**
 *
 * @author Ridwan
 */
public interface AccountRepository {
	public List<Account> getAllData();
	public Account getById(String id);
}
